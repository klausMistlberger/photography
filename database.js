'use strict'
const { genSalt } = require('bcrypt');
const bcrypt = require( 'bcrypt' );
require( 'dotenv' ).config();
const { resetMail } = require( './mailer' );

/////////////////////////////////////////////////////////////////////
// Mongo DB Atlas \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
const MongoClient = require( 'mongodb' ).MongoClient;
const dbUrl = `mongodb+srv://admin:${process.env.DB_PASSWORD}@cluster0.os60q.mongodb.net/photografy`;
// var collection;
const saltRounds = process.env.SALT_ROUNDS*1; 
const database = 'photografy';
const dbCollection = 'users';

/** read User
 * 
 * @param {string} username 
 * @returns {object} userobject
 */
const DB_userQuery = async ( username ) => {
    // console.log( 'DB_userQuery' );
    let query = { username: username };
    return new Promise( (resolve, reject) => {
        MongoClient.connect( dbUrl, async (err, client) => {
            if ( err ) reject( err );
            let db = client.db( database );
            let collection = db.collection( dbCollection );
            let querydata = await collection.find( query ).toArray();
            // console.log( 'DB querydata', querydata );
            if ( querydata.length === 0 ) resolve( false );
            if ( querydata ) resolve( querydata[0] );
            reject( 'no data' );
            client.close();
        }); // connect
    }); // promise
}; // DB_userQuery


/** create new User 
 * 
 * @param {object} reqBody 
 * @returns 
 */
const DB_createUser = async ( reqBody ) => {
    let query = { username: reqBody.newUsername };
    return new Promise( (resolve, reject) => {
        MongoClient.connect( dbUrl, async (err, client) => {
            if (err) console.error( 'DB_createUser error:', err );
            let db = client.db( database );
            let collection = db.collection( dbCollection );
            let querydata = await collection.find( query ).toArray();
            if ( querydata.length !== 0 ) {
                console.log( 'user already exists' );
                reject( 'false' );
            };
            if ( querydata.length === 0 ) {
                console.log( 'creating user...' );
                let hash = await bcrypt.hash( reqBody.newPassword, saltRounds );
                // console.log( hash );
                let dbResult = await collection.insertOne({
                    username: reqBody.newUsername,
                    email: reqBody.newEmail,
                    password: hash,
                    photos: new Array()
                });
                // console.log( dbAnswer );
                if ( dbResult.acknowledged ) {
                    console.log( 'new user created:', dbResult.acknowledged );
                    resolve( 'true' );
                };
            };
            client.close();
        }); // MongoDB connect
    }); // Promise
}; // DB_userQuery

const DB_addImage = async ( username, url, filename ) => {
    console.log( 'DB_addImage' );
    let urlParts = url.split( '/' );
    urlParts[6] = 'q_80';
    console.log( urlParts );
    let newUrl = '';
    for ( let i in urlParts ) {
        i < (urlParts.length-1) ? newUrl += urlParts[i] + '/' : newUrl += urlParts[i];
    };
    console.log( newUrl );

    return new Promise( ( resolve, reject) => {
        MongoClient.connect( dbUrl, async (err, client) => {
            if (err) reject( err );
            let db = client.db( database );
            let collection = db.collection( dbCollection );
            let score = 1;
            let imageId = Date.now();
            await collection.updateOne(
                { username: username },
                { $push:
                    { photos:
                        {
                            date: imageId,
                            username, username,
                            imageUrl: newUrl,
                            title: filename,
                            score: score
                        },
                        votes: imageId
                    }}
            );
            client.close();
            resolve({
                date: imageId,
                username, username,
                imageUrl: url,
                title: filename,
                score: score
            });
        }); // connect
    })
}; // DB_addImage

const DB_getAllImages = async () => {
    return new Promise( (resolve, reject) => {
        MongoClient.connect( dbUrl, async (err, client) => {
            if ( err ) reject( err );
            let db = client.db( database );
            let collection = db.collection( dbCollection );
            let photos = await collection.distinct( 'photos' );
            resolve( JSON.stringify( photos.reverse() ) );
            client.close();
        }); // connect
    }); // promise
}; // DB_getAllImages

const DB_updateUserMail = async ( username, newEmail ) => {
    console.log( 'DB_updateMail' );
    let query = { username: username };
    // console.log( 'DB query', query );
    return new Promise( (resolve, reject) => {
        MongoClient.connect( dbUrl, async (err, client) => {
            if ( err ) console.log( 'DB_updateUserMail error:', err );
            let db = client.db( database );
            let collection = db.collection( dbCollection );
            // let querydata = await collection.find( query ).toArray();
            // console.log( querydata[0] );
            let dbResult = await collection.updateOne(
                { username: username },
                { $set: { email: newEmail } }
            );
            if ( dbResult.acknowledged ) {
                console.log( 'email changed' );
                resolve( dbResult.acknowledged )
            } else {
                reject( dbResult.acknowledged );
            };
            client.close();
        }); // connect
    }); // Promise
}; // DB_updateUserMail


const DB_updateScore = async ( votestring, voter ) => {
    let imageCreator = votestring.split( '-' )[0];
    let imageId = votestring.split( '-' )[1] * 1;
    let vote = votestring.split( '-' )[2];
    MongoClient.connect( dbUrl, async (err, client) => {
        let db = client.db( database );
        let collection = db.collection( dbCollection );
        await collection.updateOne(
            { username: voter },
            { $push: { votes: imageId } }
        );
        await collection.updateOne(
            { "username": imageCreator, "photos": { $elemMatch: { "date": imageId } } },
            { "$inc": { "photos.$.score": vote === 'uv' ? 1 : -1 }}
        );
        client.close();
    }); // connect
}; // DB_updateScore


const DB_updatePassword = async ( username, passwordObject ) => {
    return new Promise( (resolve, reject) => {
        MongoClient.connect( dbUrl, async (err, client) => {
            if ( err ) reject( err );
            let db = client.db( database );
            let collection = db.collection( dbCollection );
            let userObject = await collection.find( {username: username} ).toArray();
            // console.log( userObject[0] );
            let result = await bcrypt.compare( passwordObject.oldPassword, userObject[0].password );
            console.log( 'old password match:', result );
            if ( result ) {
                let hash = await bcrypt.hash( passwordObject.newPassword, saltRounds );
                let dbResult = await collection.updateOne(
                    { username: username },
                    { $set: { password: hash } }
                );
                console.log( 'password changed' );
                resolve( dbResult.acknowledged );
            } else {
                console.log( 'password not changed' );
                resolve( false );
            };
            client.close();
        }); // connect
    }); // promise
}; // DB_updatePassword


const DB_updateImageName = async ( imageCreator, imageId, newImageName ) => {
    // console.log( imageCreator, imageId, newImageName );
    MongoClient.connect( dbUrl, async (err, client) => {
        let db = client.db( database );
        let collection = db.collection( dbCollection );
        await collection.updateOne(
            { "username": imageCreator, "photos": { $elemMatch: { "date": imageId } } },
            { "$set": { "photos.$.title": newImageName } }
        );
        client.close();
    }); // connect
}; // DB_updateImageName


const DB_resetPasswordRequest = async ( email ) => {
    MongoClient.connect( dbUrl, async ( err, client ) => {
        let db = client.db( database );
        let collection = db.collection( dbCollection );
        let user = await collection.find( { email: email } ).toArray();
        let token = Date.now();
        resetMail( email, user[0].username, token );
        // console.log( user[0] );
        client.close();
    });
};

const DB_resetPasswordSet = async ( email, password ) => {
    return new Promise( (resolve, reject) => {
        MongoClient.connect( dbUrl, async ( err, client ) => {
            if ( err ) { reject( err ) };
            let db = client.db( database );
            let collection = db.collection( dbCollection );
            let hash = await bcrypt.hash( password, saltRounds );
            let dbResult = await collection.updateOne(
                { email: email },
                { $set: { password: hash } }
            );
            console.log( dbResult );
            client.close();
            resolve( dbResult.acknowledged );
        });
    });
};

const DB_removeImage = async ( imageId, username ) => {
    MongoClient.connect( dbUrl, async ( err, client ) => {
        if ( err ) { console.log( err ) };
        let db = client.db( database );
        let collection = db.collection( dbCollection );
        let dbResult = await collection.updateOne(
            { "username": username },
            { $pull: { photos: { date: imageId*1 }, votes: imageId*1 } },
            { multi: true }
        );
        // console.log( dbResult );
        client.close();
    });
}; // DB_removeImage

const DB_rotateImage = async ( username, oldUrl, newUrl ) => {
    // console.log( username, oldUrl, newUrl );
    MongoClient.connect( dbUrl, async ( err, client ) => {
        if ( err ) { console.log( err ) };
        let db = client.db( database );
        let collection = db.collection( dbCollection );
        let dbResult = await collection.updateOne(
            { "username": username, "photos": { $elemMatch: { "imageUrl": oldUrl } } },
            { "$set": { "photos.$.imageUrl": newUrl } }
        );
        // console.log( dbResult );
        client.close();
    });
}; // DB_rotateImage

const DB_getUserImages = async ( username ) => {
    let query = { username:username };
    return new Promise( (resolve, reject) => {
        MongoClient.connect( dbUrl, async (err, client) => {
            if ( err ) reject( err );
            let db = client.db( database );
            let collection = db.collection( dbCollection );
            let querydata = await collection.find( query, { projection: { photos:1, _id:0 } } ).toArray();
            // console.log( 'DB querydata', querydata );
            if ( querydata.length === 0 ) resolve( false );
            if ( querydata ) resolve( querydata[0] );
            reject( 'no data' );
            client.close();
        }); // connect
    }) // Promise
}; // DB_getUserImages


module.exports = { 
    DB_userQuery,
    DB_createUser,
    DB_addImage,
    DB_getAllImages,
    DB_updateUserMail,
    DB_updateScore,
    DB_updatePassword,
    DB_updateImageName,
    DB_resetPasswordRequest,
    DB_resetPasswordSet,
    DB_removeImage,
    DB_rotateImage,
    DB_getUserImages
};

