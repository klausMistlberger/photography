# Photografy
## A platform for photography
[-> to photografy](https://photografy.herokuapp.com/)
### Frontend
- Everything frontend
### Backend
- NodeJS / Express server
- REST API
- MongoDB integration
- Cloudinary storage
