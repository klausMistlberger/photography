'use strict'
const cloudinary = require('cloudinary').v2;
const { DB_addImage } = require( './database.js' );


cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
    secure: true
});


const CLDNRY_uploadFile = async ( image ) => {
    return new Promise( (resolve, reject) =>{
        try {
            cloudinary.uploader.upload( 
                image.uploadPath, 
                {
                    // use_filename: true,
                    public_id: image.imageName.replace( /\s/g, '' ),
                    unique_filename: true,
                    exif: true,
                    format: 'jpg'
                },
                async (error, result) => {
                if (error) {
                    console.error( 'cloudinary error', error );
                    reject( error );
                } else {
                    // console.log( 'cloudinary result', result );
                    let imageObj = await DB_addImage( image.username, result.secure_url, image.imageName );
                    resolve( imageObj );
                };
            });
        }
        catch ( error ) {
            console.error( error ); 
            reject( error );
        };
    }); // promise
    
}; // uploadFile

const CLDNRY_deleteImage = async ( publicId ) => {
    console.log( 'cloudinary deleteImage:', publicId );
    let cloudinaryResponse = await cloudinary.uploader.destroy( publicId );
    // console.log( cloudinaryResponse );
};


module.exports = { 
    CLDNRY_uploadFile,
    CLDNRY_deleteImage
};