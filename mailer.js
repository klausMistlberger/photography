'use strict'
require( 'dotenv' ).config();
const nodemailer = require( 'nodemailer' );

let transporter = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: process.env.MAILER_USER, 
        pass: process.env.MAILER_PASS, 
    }
});

/**
 * 
 * @param {string} mailto 
 * @param {string} user 
 */
const creationMail = async ( mailto, user ) => {

    let message = `<b>Welcome to Photografy, ${user}!</b><br>
    <p>Thanks for signing up with us.</p>
    <p>Your Photografy Team</p>
    <a href="http://photografy.herokuapp.com"><h2>To Photografy<h2></a>
    `;

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: process.env.MAILER_SENDER,
        to: mailto, 
        subject: "Photografy account created",
        text: "Welcome to Photografy",
        html: message,
    });

    // console.log( info );
    console.log("Message sent: %s", info.messageId);
};

/**
 * 
 * @param {'string'} mailto 
 * @param {'string'} username 
 */
const resetMail = async ( mailto, user, token ) => {

    let message = `<b></b><br>
    <p><b>Hello ${user}!</b></p>
    <p>Here is your password reset link:</p>
    <a href="http://photografy.herokuapp.com/resetPassword/${token}/${mailto}"><h2>Set new password</h2></a>
    <p>This link is valid for 5 minutes.</p>
    <p>Your Photografy Team</p>
    `;

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: process.env.MAILER_SENDER,
        to: mailto, 
        subject: "Photografy account reset",
        text: "Photografy account reset",
        html: message,
    });

    console.log("Message sent: %s", info.messageId);
};

module.exports = {
    creationMail,
    resetMail
};
