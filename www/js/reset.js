'use strict'
let queryString = window.location.search.split( '?' );
console.log( queryString );
queryString.shift();
let token = queryString[0].split( '=' );
let email = queryString[1].split( '=' );
let time = ( Date.now() - token[1] ) / 60 / 1000;
console.log( 'time', Math.round( time ) );

_e( 'sendNewPassword' ).onclick = ev => {
    ev.preventDefault();
    let queryString = window.location.search.split( '?' );
    queryString.shift();
    let token = queryString[0].split( '=' );
    let email = queryString[1].split( '=' );
    time = ( Date.now() - token[1] ) / 60 / 1000;

    const sendResetPassword = () => {
        let resetNewPassword = _e( 'resetNewPassword' );
        let reRepeatNewPassword = _e( 'reRepeatNewPassword' );
        if ( resetNewPassword.value === reRepeatNewPassword.value ) {
            let ResetLoader = new AjaxLoader();
            let setPassword = new FormData();
            setPassword.append( 'newPassword', resetNewPassword.value );
            setPassword.append( 'email', email[1] );
            setPassword.append( 'token', token[1] );
            _e( 'reRepeatNewPassword' ).classList.remove( 'is-invalid' );

            axios({
                method: 'post',
                url: '/setNewPassword',
                data: setPassword
            })
            .then( res => {
                ResetLoader.removeLoader();
                _e( 'newPasswordForm' ).style.display = 'none';
            });

        } else {
            _e( 'reRepeatNewPassword' ).classList.add( 'is-invalid' );
        };
    };
    if ( (Date.now()-token[1]) >= 300000 ) {
        console.log( 'invalid' );
        // invalid UI here 
    } else {
        console.log( 'valid' );
        sendResetPassword();
    };
}; // sendNewPassword
