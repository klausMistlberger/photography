'use strict';
/////////////////////////////////////////////////////////////////////
// CONTENTLOADER \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
class Contentloader {

    constructor( user ) {
        this.user = user;
        this.sceenResolution = { width:window.innerWidth, height:window.innerHeight };
        this.allUrls = [];
        this.userUrls = user.photos.reverse(); 
        this.showUserUrls = [];
        this.date;
        this.username = user.username; 
        this.imageUrl = '';
        this.title = '';
        this.score = 1;

        this.votes = [];

        this.position; // window.pageYOffset
        this.fullHeight; // height of Document
        this.imageCount = 0; 

        this.initializeContentLoader(); 
    };

    async initializeContentLoader() {
        console.log( 'contentLoader initialized' );
        if ( this.user.votes ) this.votes = this.user.votes;
        this.getScreenResolution();
        this.getUserImageUrls();
        this.getAllImageUrls();
        this.loadNewContent();
    };

    getUserImageUrls() {
        console.log( 'user images loaded' );
    };

    getAllImageUrls() {
        let AllImageLoader = new AjaxLoader();
        axios({
            method: 'get',
            url: '/member/getAllImages',
            params: { username: this.username }
        })
        .then( async res => {
            if ( res.data != '' ) {
                this.allUrls = await res.data;
                console.log( 'all images loaded' );
                this.showImages( this.allUrls.slice( this.imageCount, this.imageCount+10 ), 'mainfotowrapper' );
                this.imageCount += 10; 
                this.fullHeight = document.body.clientHeight;
            } else {
                console.log( 'all: no images yet' );
            };
            AllImageLoader.removeLoader();
        })
        .catch( err => console.log( err ));
    };

    
    /**
     * 
     * @param {'this.variable'} url 
     * @param {'string'} container 
     */
    showImages( url, container ) {
        url.map( (el,ix) => {
            for ( let i in el ) {
                this.date = el.date;
                this.username = el.username;
                this.imageUrl = el.imageUrl;
                this.title = el.title;
                this.score = el.score;
            }
            this.buildImageContainer( container );
        });
        this.removeVoteArrows( container );
    };


    async buildImageContainer( container ) {
        let section = _ce( 'section' );
        section.classList.add( 'contentSection' );
        section.innerHTML = `
        <header class="contentHeader" id="hId${container}${this.date}">
            <div class="headerInfo">${this.title}</div>
        </header>
        <hr>
        <div class="contentImage">
            <a href="" class="imagelink"><img loading="lazy" src="${this.imageUrl}" alt=""></a>
        </div>
        <hr>
        <footer class="contentFooter">
            <div class="footerInfo"><a href="" class="toUserPage">${this.username}</a></div>
            <div class="uvdv" id="${container}${this.date}">
                <div class="votebutton arrow arrow-top upvote" data-i="${this.username}-${this.date}-uv"></div>
                <div class="votebutton arrow arrow-bottom downvote" data-i="${this.username}-${this.date}-dv"></div>
                <span class="footerCounter">${this.score}</span>
            </div>
        </footer>`;
        _e( container ).appendChild( section );

        if ( container === 'myfotowrapper' ) {
            let edits = _ce( 'a' );
            edits.setAttribute( 'href', '' );
            edits.id = `edit-${this.date}`;
            edits.classList.add( 'editImage' );
            edits.innerHTML = 'edit';

            _e( `hId${container}${this.date}` ).appendChild( edits );
        };

        // Voting
        _qsa( '.votebutton' ).forEach( el => {
            el.onclick = async ev => {
                ev.preventDefault();
                let voteString = el.getAttribute( 'data-i' );
                let voteUser = el.getAttribute( 'data-i' ).split( '-' )[0];
                let voteDate = el.getAttribute( 'data-i' ).split( '-' )[1];
                let vote = el.getAttribute( 'data-i' ).split( '-' )[2];
                let voteScore = el.parentNode.querySelector( '.footerCounter' ).innerHTML * 1;
                await axios({
                    method: 'put',
                    url: '/member/updateScore',
                    params: { vote: voteString }
                })
                switch ( vote ) {
                    case 'uv':
                        voteScore++;
                        el.parentNode.querySelector( '.footerCounter' ).innerHTML = voteScore;
                        el.parentNode.querySelector( '.downvote' ).remove();
                        el.remove();

                    break;
                    case 'dv':
                        voteScore--;
                        el.parentNode.querySelector( '.footerCounter' ).innerHTML = voteScore;
                        el.parentNode.querySelector( '.upvote' ).remove();
                        el.remove();

                    break;
                    default:
                        console.log( 'contentloader: vote default: this should not have happend...' );
                    break;
                };
            }; // onclick
        }); // querySelectorAll .votebutton

        _qsa( '.toUserPage' ).forEach( el => {
            el.onclick = ev => {
                ev.preventDefault();
                if ( ev.target.innerHTML === this.user.username ) {
                    _e( 'mainfotowrapper' ).style.display = 'none';
                    this.backToMyImages();
                    window.scrollTo({ top: 0, left: 0, behavior: 'instant' });
                } else {
                    _e( 'userfotowrapper' ).innerHTML = '';
                    axios({
                        method: 'get',
                        url: '/member/showUserImages',
                        params: { username:ev.target.innerHTML }
                    })
                    .then( res => {
                        this.showUserUrls = res.data;
                        _e( 'mainfotowrapper' ).style.display = 'none';
                        _e( 'userfotowrapper' ).style.display = 'flex';
                        _qs( 'h2' ).innerHTML = `${ev.target.innerHTML}'s Images`
                        this.showImages( this.showUserUrls, 'userfotowrapper' );
                        window.scrollTo({ top: 0, left: 0, behavior: 'instant' });
                    })
                    .catch( err => console.error( err ) );
                };
            }; // onclick
        }); // toUserPage


        _qsa( '.editImage' ).forEach( el => {
            el.onclick = ev => {
                ev.preventDefault();
                let imageId = el.id.split( '-' )[1];
                let name = el.parentNode.querySelector( '.headerInfo' ).innerHTML; 
                let imageLink = el.parentNode.parentNode.querySelector( '[src]' ).src;

                _e( 'myfotowrapper' ).style.display = 'none';
                _e( 'helloHeader' ).innerHTML = `Edit image`;
                _e( 'editwrapper' ).style.display = 'flex';
                _e( 'containEditImage' ).innerHTML = `<img src="${imageLink}" id="iId-${imageId}">`;

                _e( 'changeImageName' ).value = name;

                _e( 'changeImage' ).onclick = ev => {
                    ev.preventDefault();
                    let newImageName = _e( 'changeImageName' ).value.trim();
                    _e( 'changeImageName' ).classList.remove( 'is-invalid' );
                    if ( newImageName.length > 42 ) {
                        _e( 'changeImageName' ).classList.add( 'is-invalid' );
                    } else {
                        let ChangeLoader = new AjaxLoader();
                        let photo = MEMBER.user.photos.find( photo => photo.date == imageId );
                        photo.title = newImageName;

                        axios({
                            method: 'put',
                            url: '/member/changeImage',
                            params: { newImageName: newImageName, imageId: imageId }
                        })
                        .then( res => {
                            ChangeLoader.removeLoader();
                            this.backToMyImages();
                        })
                        .catch( err => console.error( err ));
                    };
                    
                };

                _e( 'backToMy' ).onclick = ev => {
                    ev.preventDefault();
                    this.backToMyImages();
                };

                _e( 'deleteImage' ).onclick = ev => {
                    ev.preventDefault();
                    let imageId = _e( 'containEditImage' ).querySelector( 'img' ).getAttribute( 'id' ).split( '-' )[1];
                    let urlParts = _qs( ' #containEditImage img' ).getAttribute( 'src' ).split( '/' );
                    let publicId = urlParts[urlParts.length-1].split( '.' )[0];
                    let DeleteLoader = new AjaxLoader();
                    axios({
                        method: 'delete',
                        url: '/member/deleteImage',
                        params: { publicId: publicId, imageId: imageId }
                    })
                    .then( res => {
                        let image = this.userUrls.find( photo => photo.date === imageId*1 );
                        this.userUrls.splice( this.userUrls.indexOf( image ), 1 );
                        DeleteLoader.removeLoader();
                        this.backToMyImages();
                    })
                    .catch( err => console.log( err ) );
                };
            };
        }); // editImage

        // Fullscreen Image
        _qsa( '.imagelink' ).forEach( el => {
            el.onclick = ev => {
                ev.preventDefault();
                let link = ev.target.getAttribute( 'src' );
                let imageview = _ce( 'img' );
                imageview.id = 'imageview';
                imageview.src = link;

                let photoviewer = _ce( 'div' );
                photoviewer.id = 'photoviewer';
                _e( 'all' ).appendChild( photoviewer );
                _e( 'photoviewer' ).appendChild( imageview );

                _e( 'photoviewer' ).onclick = ev => {
                    _e( 'imageview' ).remove();
                    _e( 'photoviewer' ).remove();
                    imageview = '';
                    photoviewer = '';
                };
            };
        });
        // this.voted();
    }; // buildImageContainer

    
    backToMyImages() {
        _e( 'editwrapper' ).style.display = 'none';
        _e( 'helloHeader' ).innerHTML = `Hello ${this.user.username}`;
        _e( 'myfotowrapper' ).innerHTML = '';
        this.showImages( this.userUrls, 'myfotowrapper' );
        _e( 'myfotowrapper' ).style.display = 'flex';
    };

    /**
     * remove vote-arrow for user who already voted for an image
     */
    removeVoteArrows( cw ) {
        if ( this.votes.length != 0 ) {
            this.votes.forEach( el => {
                if ( _e( cw+el ) ) {
                    _e( (cw+el) ).querySelectorAll( 'div' ).forEach( el => el.remove() );
                };
            });
        };
    }; // voted

    getScreenResolution() {
        window.onresize = (ev) => {
            this.sceenResolution.width = window.innerWidth;
            this.sceenResolution.height = window.innerHeight;
        };
    };

    loadNewContent() {
        window.onscroll = (ev) => {
            let mainwrapperDisplay = window.getComputedStyle( _e( 'mainwrapper' ) ).display;
            let mainfotowrapperDisplay = window.getComputedStyle( _e( 'mainfotowrapper' ) ).display;
            if ( mainwrapperDisplay === 'flex' && mainfotowrapperDisplay === 'flex' ) {
                this.fullHeight = document.body.clientHeight;
                this.position = window.scrollY;
                if ( (this.fullHeight-this.position) < 3000 && this.imageCount < this.allUrls.length ) {
                    console.log( 'loadNewContent' );
                    this.showImages( this.allUrls.slice( this.imageCount, this.imageCount+10 ), 'mainfotowrapper' );
                    this.imageCount += 10; 
                };
            };
        };
    }; // loadNewContent()
}; // Contentloader