'use strict'
/////////////////////////////////////////////////////////////////////
// CONTENTLOADER \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
class AjaxLoader {

    constructor() {
        this.screenX = window.innerWidth;
        this.screenY = window.innerHeight;
        this.x = 50;
        this.y = 50;
        this.r = 10;
        this.w = 5;
        this.speed = 0.01*3;
        this.canvas;
        this.ajaxloader;

        this.initLoader();
    };

    initLoader() {
        this.canvas = _ce( 'canvas' );
        let ctx = this.canvas.getContext( '2d' );
        let a = 0;
        this.canvas.width = 100;
        this.canvas.height = 100;
        this.canvas.style.width = '100px';
        this.canvas.style.height = '100px';

        setInterval( () => {
            // console.log( this.a );
            a = a + this.speed; 
            let b = a + 2;

            let a2 = a*2*-1;
            let b2 = a2 + 2;

            let a3 = a*3;
            let b3 = a3 + 2;

            let a4= a*4*-1;
            let b4 = a4 + 2;

            ctx.clearRect( 0,0,100,100);

            ctx.beginPath();
            ctx.lineWidth = this.w; 
            ctx.strokeStyle = '#775f5f'; 
            ctx.arc( this.x, this.y, this.r, a4, b4 );
            ctx.stroke();
            ctx.closePath();

            ctx.beginPath();
            ctx.lineWidth = this.w; 
            ctx.strokeStyle = '#5f5f77'; 
            ctx.arc( this.x, this.y, this.r*2, a3, b3 ); 
            ctx.stroke();
            ctx.closePath();

            ctx.beginPath();
            ctx.lineWidth = this.w; 
            ctx.strokeStyle = '#775f5f'; 
            ctx.arc( this.x, this.y, this.r*3, a2, b2 );
            ctx.stroke();
            ctx.closePath();

            ctx.beginPath();
            ctx.lineWidth = this.w; 
            ctx.strokeStyle = '#607867'; 
            ctx.arc( this.x, this.y, this.r*4, a, b );
            ctx.stroke();
            ctx.closePath();

            if ( a > 2*Math.PI ) { a = 0 };

            // console.log( this.x, this.y, this.w, this.r );
        }, 40 )

        this.addLoader();

    };

    addLoader() {
        this.ajaxloader = _ce( 'div' );
        this.ajaxloader.id = 'ajaxloader';
        this.ajaxloader.style.width = this.screenX;
        this.ajaxloader.style.height = this.screenY;

        _e( 'all' ).appendChild( this.ajaxloader );
        _e( 'ajaxloader' ).appendChild( this.canvas );
    };

    removeLoader() {
        _e( 'ajaxloader' ).removeChild( this.canvas );
        _e( 'all' ).removeChild( this.ajaxloader );
    };  
};
