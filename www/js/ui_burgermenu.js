'use strict';
/////////////////////////////////////////////////////////////////////
// Burgermenu \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////

// Burgermenu Visibility \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
var menucount = 0;
var position;
_e( 'bmenu' ).onclick = () => {
    menucount++;

    switch ( menucount%2 ) {
        case 1:
            position = window.scrollY;
            if ( _e( 'loginwrapper' ) ) _e( 'loginwrapper' ).style.display = 'none';
            if ( _e( 'mainwrapper' ) ) _e( 'mainwrapper' ).style.display = 'none';
            _e( 'menu' ).style.display = 'flex';
        break;
        case 0:
            if ( _e( 'loginwrapper' ) ) _e( 'loginwrapper' ).style.display = 'flex';
            if ( _e( 'mainwrapper' ) ) _e( 'mainwrapper' ).style.display = 'flex';
            _e( 'menu' ).style.display = 'none';
            window.scrollTo({ top: position, left: 0, behavior: 'instant' });
        break;
        default: 
            console.log( 'default' );
        break;

    }
    menucount >= 2 ? menucount = 0 : menucount; 
};
