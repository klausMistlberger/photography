'use strict';
class Member {

    constructor( username ) {
        this.username = username;
        this.user = {}; 
        this.Loader;
        this.initializeMember();
    };

    async initializeMember() {
        _qs( 'h2' ).innerHTML = `Hello ${this.username}`;
        await this.getUser();
    };

    async getUser() {
        axios({
            method: 'get',
            url: '/member/getUser',
            params: { username: this.username }
        })
        .then( async res => {
            this.user = await res.data;
            this.Loader = new Contentloader( this.user );
        });

    };

    consoleLogs() {
        console.log( this.username );
        console.log( this.userObject );
    };

};