'use strict';
const emailRegEx = /[a-zA-Z0-9\.]+[@][a-zA-Z0-9]{2,}\.[a-z]{2,}/; 
const resetForms = () => {
    _qsa( '#loginwrapper input' ).forEach( el => {
        el.classList.remove( 'is-invalid' );
        el.value = '';
    });
};

/////////////////////////////////////////////////////////////////////
// Login \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
_e( 'bmenuLogin' ).onclick = ev => {
    ev.preventDefault();
    menucount = 0;
    _e( 'menu' ).style.display = 'none';
    _e( 'loginwrapper' ).style.display = 'flex';
    toLogin();

};

/////////////////////////////////////////////////////////////////////
// Register \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
_e( 'bmenuRegister' ).onclick = ev => {
    ev.preventDefault();
    menucount = 0;
    _e( 'menu' ).style.display = 'none';
    _e( 'showLogin' ).style.display = 'none';
    _e( 'loginwrapper' ).style.display = 'flex';
    _e( 'showRegister' ).style.display = 'flex';
    _e( 'aboutwrapper' ).style.display = 'none';
};

_e( 'bmenuAbout' ).onclick = ev => {
    ev.preventDefault();
    menucount = 0;
    _e( 'menu' ).style.display = 'none';
    _e( 'aboutwrapper' ).style.display = 'flex';
    _e( 'loginwrapper' ).style.display = 'flex';
    _e( 'showLogin' ).style.display = 'none';
    _e( 'showRegister' ).style.display = 'none';
};

const toLogin = () => {
    _e( 'showLogin' ).style.display = 'flex';
    _e( 'showRegister' ).style.display = 'none';
    _e( 'showForgotPw' ).style.display = 'none';
    _e( 'aboutwrapper' ).style.display = 'none';
};


// #region LoginForms Inputs
let login = () => {
    let LoginLoader = new AjaxLoader();
    let username = _e( 'username' );
    let password = _e( 'password' );
    username.classList.remove( 'is-invalid' );
    password.classList.remove( 'is-invalid' );

    if ( username.value.length > 4 && password.value.length > 7 ) {
        console.info( 'client login validated' );

        let loginData = new FormData();
        loginData.append( 'username', username.value.trim() );
        loginData.append( 'password', password.value );
        
        axios({
            method: 'post',
            url: '/login',
            data: loginData
        }).then( (res) => {
            LoginLoader.removeLoader();
            localStorage.setItem( 'user', JSON.stringify( res.data.user ) ); // userData to ui.js
            window.location = res.data.target;
        }).catch( err => {
            LoginLoader.removeLoader();
            username.classList.add( 'is-invalid' );
            password.classList.add( 'is-invalid' );
        });
        
    } else {
        LoginLoader.removeLoader();
        if ( username.value.length <= 4 ) {
            username.classList.add( 'is-invalid' );
        };
        if ( password.value.length <= 7 ) {
            password.classList.add( 'is-invalid' );
        };
    };
}; // login()


_e( 'sendLogin' ).onclick = ev => {
    ev.preventDefault();
    login();
};


let register = () => {
    let usernameRegex = /^[a-zA-Z0-9]{5,25}/;
    let newUsername = _e( 'newUsername' );
    let newEmail = _e( 'newEmail' );
    let newPassword = _e( 'newPassword' );
    let repeatNewPassword = _e( 'repeatNewPassword' );
    
    newUsername.classList.remove( 'is-invalid' );
    newEmail.classList.remove( 'is-invalid' );
    newPassword.classList.remove( 'is-invalid' );
    repeatNewPassword.classList.remove( 'is-invalid' );


    if (newUsername.value.match( usernameRegex) && newUsername.value.length > 4 && newPassword.value.length > 7 && newPassword.value === repeatNewPassword.value && newEmail.value.match( emailRegEx ) ) {
        let RegisterLoader = new AjaxLoader();
        console.log( 'valid registration' );
        
        let registerData = new FormData();
        registerData.append( 'newUsername', newUsername.value.trim() );
        registerData.append( 'newEmail', newEmail.value.trim() );
        registerData.append( 'newPassword', newPassword.value.trim() );

        axios({
            method: 'post',
            url: '/register',
            data: registerData
        }).then( res => {
            RegisterLoader.removeLoader();
            // console.log( typeof res );
            if ( res.data === true ) {
                // toLogin();
                resetForms();
                _e( 'showRegister' ).style.display = 'none';
                _e( 'registerThanks' ).style.display = 'flex';
            };
            if ( res.data === false ) {
                _e( 'regusernamefeedback' ).innerHTML = 'username already in use';
                _e( 'newUsername' ).classList.add( 'is-invalid' );
            };
        }).catch( err => {
            RegisterLoader.removeLoader();
            console.error( 'xhr error register', err );
        });

    } else {
        console.log( 'registration data not valid' );

        if ( newUsername.value.length <= 4 ) {
            _e( 'regusernamefeedback' ).innerHTML = 'must be at least 5 characters long, no special characters allowed';
            newUsername.classList.add( 'is-invalid' );
        };
        if ( !newEmail.value.match( emailRegEx ) ) {
            newEmail.classList.add( 'is-invalid' );
        };
        if ( newPassword.value <= 7 ) {
            newPassword.classList.add( 'is-invalid' );
        };
        if ( repeatNewPassword.value != newPassword.value ) {
            repeatNewPassword.classList.add( 'is-invalid' );
        };
    };
}; // register()


_e( 'sendRegister' ).onclick = ev => {
    ev.preventDefault();
    register();
};

_e( 'registerThanks' ).onclick = ev => {
    ev.preventDefault();
    _e( 'registerThanks' ).style.display = 'none';
    toLogin();
};


_e( 'pwReset' ).onclick = ev => {
    ev.preventDefault();
    let pwResetEmail = _e( 'pwResetEmail' );
    if( pwResetEmail.value.match( emailRegEx ) ) {
        let ResetLoader = new AjaxLoader();

        axios({
            method: 'get',
            url: '/pwReset',
            params: { reset: pwResetEmail.value }
        })
        .then( res => {
            ResetLoader.removeLoader();
            _e( 'showForgotPw' ).querySelectorAll( 'div' ).forEach( el => {
                el.style.display = 'none';
            });
            let message = _ce( 'div' );
            message.id = 'resetmessage';
            message.innerHTML = `If your email is registered you will receive a password reset link at ${pwResetEmail.value}`;
            _e( 'showForgotPw' ).appendChild( message );
        })
        .catch ( err => console.error( err ) );
    };
};

// #endregion 

// #region LoginForms Navigation
_e( 'createAccount' ).onclick = ev => {
    ev.preventDefault();
    _e( 'showLogin' ).style.display = 'none';
    _e( 'showRegister' ).style.display = 'flex';
};

_e( 'forgotPw' ).onclick = ev => {
    ev.preventDefault();
    _e( 'showLogin' ).style.display = 'none';
    _e( 'showForgotPw' ).style.display = 'flex';
};

_qsa( '.toLogin' ).forEach( el => {
    el.onclick = ev => {
        ev.preventDefault();
        resetForms();
        toLogin();
    };
});
// #endregion