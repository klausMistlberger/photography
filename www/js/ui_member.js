'use strict';
/////////////////////////////////////////////////////////////////////
// Fileupload \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
console.log( 'member logged in' );
// sessionStorage in login.js
let user = JSON.parse( localStorage.getItem( 'user' ) );
let MEMBER = new Member( user ); 

/////////////////////////////////////////////////////////////////////
// Burgermenu All Photos  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
_e( 'feed' ).onclick = ev => { 
    ev.preventDefault();
    menucount = 0; 
    window.location = '/member.html';
};

// Upload \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
_e( 'upload' ).onclick = ev => {
    ev.preventDefault();
    menucount = 0; 
// UI \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    _qs( 'h2' ).innerHTML = 'Upload Image';
    _e( 'mainwrapper' ).style.display = 'flex';
    _e( 'menu' ).style.display = 'none';
    _e( 'mainfotowrapper' ).style.display = 'none';
    _e( 'myfotowrapper' ).style.display = 'none';
    _e( 'accountwrapper' ).style.display = 'none';
    _e( 'fileupload' ).style.display = 'flex';
    _e( 'uploadsuccess' ).style.display = 'none'; 
    _e( 'editwrapper' ).style.display = 'none';
    _e( 'aboutwrapper' ).style.display = 'none';


// Input \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    function handleFile( ev, image ) {
    // UI \\
        _e( 'inputUpload' ).style.display = 'none';
        _e( 'submitUpload' ).style.display = 'initial';
        _e( 'naming' ).style.display = 'flex';
        _e( 'dropzone' ).innerHTML = image.name.split( '.' )[0];

    // File \\
        _e( 'submitUpload' ).onclick = ev => {
            ev.preventDefault();
            let imageData = new FormData();
            let imageName = _e( 'imageName' ).value ? _e( 'imageName' ).value : image.name.split( '.' )[0];
            if ( !imageName.match( /^[a-zA-Z0-9\ ]{1,42}$/) ) {
                _e( 'imageName' ).classList.add( 'is-invalid' );
            } else {
                let UploadLoader = new AjaxLoader();
                _e( 'imageName' ).classList.remove( 'is-invalid' );
                imageData.append( 'image', image );
                imageData.append( 'imageName', imageName.trim() );
                imageData.append( 'username', MEMBER.username );
    
                axios({
                    url: '/member/fileupload',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: imageData
                })
                .then( (res) => {
                    MEMBER.Loader.userUrls.unshift( res.data );
                    MEMBER.Loader.votes.push( res.data.date );

                    _e( 'uploadsuccess' ).style.display = 'initial'; 
                    _e( 'fileupload' ).style.display = 'none';
                    _e( 'inputUpload' ).style.display = 'initial';
                    _e( 'submitUpload' ).style.display = 'none';
                    _e( 'naming' ).style.display = 'none';
    
                    _e( 'dropzone' ).innerHTML = '...drop zone...';
                    _e( 'inputUpload' ).value = '';
    
                    UploadLoader.removeLoader();
                })
                .catch( (err) => {
                    console.error( err );
                    UploadLoader.removeLoader();
                }); // axios
            }; // else
        }; // submit upload onclick
    }; // handleFile

    _e( 'imageUploadForm' ).onchange = ev => {
        let image = ev.target.files[0];
        handleFile( ev, image );
    };

// Dropzone \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    let dropzone = _e( 'dropzone' ); 
    dropzone.ondragover = ev => {
        ev.preventDefault();
    };
    
    dropzone.ondrop = ev => {
        ev.preventDefault();
        let image = ev.dataTransfer.files[0];
        handleFile( ev, image );
    };
}; // upload

/////////////////////////////////////////////////////////////////////
// Burgermenu My Photos  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
_e( 'myphotos' ).onclick = ev => {
    ev.preventDefault();
    menucount = 0;
    _qs( 'h2' ).innerHTML = `Hello ${MEMBER.username}`;
    _e( 'mainwrapper' ).style.display = 'flex';
    _e( 'myfotowrapper' ).innerHTML = '';
    _e( 'menu' ).style.display = 'none';
    _e( 'fileupload' ).style.display = 'none';
    _e( 'mainfotowrapper' ).style.display = 'none';
    _e( 'accountwrapper' ).style.display = 'none';
    _e( 'myfotowrapper' ).style.display = 'flex';
    _e( 'editwrapper' ).style.display = 'none';
    _e( 'aboutwrapper' ).style.display = 'none';

    MEMBER.Loader.showImages( MEMBER.Loader.userUrls, 'myfotowrapper' );

    if ( MEMBER.Loader.userUrls.length === 0 ) {
        let noImages = _ce( 'div' );
        noImages.id = 'noImages';
        noImages.innerHTML = 'You haven\'t uploaded anything yet.'
        _e( 'myfotowrapper' ).appendChild( noImages );
        let uploadImageButton = _ce( 'button' );
        uploadImageButton.id = 'uploadImageButton';
        uploadImageButton.innerHTML = 'Upload Image';
        uploadImageButton.classList.add( 'btn', 'btn-secondary', 'accountbuttons' );
        _e( 'myfotowrapper' ).appendChild( uploadImageButton );

        _e( 'uploadImageButton' ).onclick = ev => {
            ev.preventDefault();
            _qs( 'h2' ).innerHTML = 'Upload Image';
            _e( 'fileupload' ).style.display = 'flex';
            _e( 'myfotowrapper' ).style.display = 'none';
        };

    };
}; // myphotos onclick

/////////////////////////////////////////////////////////////////////
// Burgermenu Account  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
_e( 'account' ).onclick = ev => {
    ev.preventDefault();
    menucount = 0; 
    _qs( 'h2' ).innerHTML = `Account overview`;
    _e( 'mainwrapper' ).style.display = 'flex';
    _e( 'menu' ).style.display = 'none';
    _e( 'fileupload' ).style.display = 'none';
    _e( 'mainfotowrapper' ).style.display = 'none';
    _e( 'accountwrapper' ).style.display = 'flex';
    _e( 'myfotowrapper' ).style.display = 'none';
    _e( 'editwrapper' ).style.display = 'none';
    _e( 'aboutwrapper' ).style.display = 'none';

    _e( 'usernameUpdate' ).value = MEMBER.username;
    _e( 'emailUpdate' ).value = MEMBER.user.email;
};

_e( 'changeUserMail' ).onclick = ev => {
    ev.preventDefault();
    let MailChangeLoader = new AjaxLoader();
    let updateEmail = _e( 'emailUpdate' ).value;
    axios({
        method: 'put',
        url: '/member/updateUser',
        params: { updateEmail: updateEmail }
    })
    .then( (res) => {
        if ( res.data ) {
            console.log( 'email changed' );
        } else {
            console.log( 'email not changed' );
        };
        MailChangeLoader.removeLoader();
    })
    .catch( err => console.log( 'mail change error:', err) );
};

_e( 'changePassword' ).onclick = ev => {
    ev.preventDefault();
    _e( 'oldPassword' ).classList.remove( 'is-invalid' );
    _e( 'newPassword' ).classList.remove( 'is-invalid' );
    _e( 'repeatNewPassword' ).classList.remove( 'is-invalid' );
    let oldPassword = _e( 'oldPassword' );
    let newPassword = _e( 'newPassword' );
    let repeatNewPassword = _e( 'repeatNewPassword' );

    if ( ( newPassword.value === repeatNewPassword.value ) && ( newPassword.value.length > 7 ) ) {
        let passwordData = new FormData();
        passwordData.append( 'username', MEMBER.username );
        passwordData.append( 'oldPassword', oldPassword.value );
        passwordData.append( 'newPassword', newPassword.value );
        passwordData.append( 'repeatNewPassword', repeatNewPassword.value );
        
        let PasswordChangeLoader = new AjaxLoader();
        axios({
            method: 'post',
            url: '/member/changePassword',
            data: passwordData
        })
        .then( res => {
            PasswordChangeLoader.removeLoader();
            console.log( res.data );
            if ( res.data == true ) {
                _e( 'oldPassword' ).value = '';
                _e( 'newPassword' ).value = '';
                _e( 'repeatNewPassword' ).value = '';
            };

            if ( res.data == false ) {
                _e( 'oldPassword' ).classList.add( 'is-invalid' );
            };
        })
        .catch( err => {
            console.log( err );
        });
    };

    if ( newPassword.length < 8 ) {
        _e( 'newPassword' ).classList.add( 'is-invalid' );
    };

    if ( newPassword.value !== repeatNewPassword.value ) {
        _e( 'repeatNewPassword' ).classList.add( 'is-invalid' );
    };

};

_e( 'toDelete' ).onclick = ev => {
    ev.preventDefault();
    _e( 'userChange' ).style.display = 'none';
    _e( 'passwordChange' ).style.display = 'none';
    _e( 'deletewrapper' ).style.display = 'initial';
};

_e( 'deleteAccount' ).onclick = ev => {
    ev.preventDefault();
};

_e( 'noNotDelete' ).onclick = ev => {
    ev.preventDefault();
    _e( 'userChange' ).style.display = 'initial';
    _e( 'passwordChange' ).style.display = 'initial';
    _e( 'deletewrapper' ).style.display = 'none';
};

/////////////////////////////////////////////////////////////////////
// Burgermenu About  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
_e( 'about' ).onclick = ev => { 
    ev.preventDefault();
    menucount = 0; 
    _qs( 'h2' ).innerHTML = 'About Photografy';
    _e( 'mainwrapper' ).style.display = 'flex';
    _e( 'aboutwrapper' ).style.display = 'flex';
    _e( 'menu' ).style.display = 'none';
    _e( 'fileupload' ).style.display = 'none';
    _e( 'mainfotowrapper' ).style.display = 'none';
    _e( 'accountwrapper' ).style.display = 'none';
    _e( 'myfotowrapper' ).style.display = 'none';
    _e( 'editwrapper' ).style.display = 'none';
};

/////////////////////////////////////////////////////////////////////
// Burgermenu Logout  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
_e( 'logout' ).onclick = () => {
    localStorage.removeItem( 'user' );
    axios({
        method: 'get',
        url: '/logout'
    })
    .then( res => console.log( res, 'logged out' ) )
    .catch( err => console.log( err ) );
};

/////////////////////////////////////////////////////////////////////
// Rotate Image \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
const buildNewUrl = async ( imageUrl, urlParts ) => {
    let newUrl = '';
    for ( let i in urlParts ) {
        i < (urlParts.length-1) ? newUrl += urlParts[i] + '/' : newUrl += urlParts[i];
    };
    let image = MEMBER.Loader.userUrls.find( url => url.imageUrl === imageUrl );
    image.imageUrl = newUrl;
    await _qs( ' #containEditImage img' ).setAttribute( 'src', newUrl );
    let RotateLoader = new AjaxLoader();
    axios({
        method: 'put',
        url: '/member/rotateImage',
        params: { oldUrl: imageUrl, newUrl: newUrl }
    })
    .then( res => {
        RotateLoader.removeLoader();
    })
    .catch( err => console.error( err ) );
    // console.log( image );
}; // buildNewUrl

const rotateImage = ( angle ) => {
    let rotationRegex = /^q_70,a_-?[0-9]{1,3}/;
    let imageUrl = _qs( ' #containEditImage img' ).getAttribute( 'src' );
    let urlParts = imageUrl.split( '/' );
    if ( !urlParts[6].match( rotationRegex ) ) {
        urlParts[6] = `q_70,a_${angle}`;
        buildNewUrl( imageUrl , urlParts );
    } else {
        let newAngle = urlParts[6].split( ',' )[1].split( '_' )[1]*1 + angle;
        if ( newAngle === 360 || newAngle === -360 ) newAngle = 0;
        urlParts[6] = `q_70,a_${newAngle}`;
        buildNewUrl( imageUrl , urlParts );
    };
}; // rotateImage


_qsa( '.rotateArrow' ).forEach( el => {
    el.onclick = ev => {
        if ( el.classList.contains( 'rotateLeft' ) ) {
            rotateImage( -90 );
        };
        if ( el.classList.contains( 'rotateRight' ) ) {
            rotateImage( 90 );
        };
    };
});