'use strict'
// console.log( 'helper.js loaded' );
/////////////////////////////////////////////////////////////////////
// Helper  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
/** getElementById
 * 
 * @param {'string'} getElementById
 * @returns HTMLElement
 */
function _e( id ) {
    return document.getElementById( id );
};

/** querySelector 
 * 
 * @param {'string'} querySelector
 * @returns 
 */
function _qs( qS ) {
    return document.querySelector( qS );
};

/** querySelectorAll
 * 
 * @param {'string'} querySelectorAll
 * @returns 
 */
function _qsa( qSA ) {
    return document.querySelectorAll( qSA );
};

/** createElement
 * 
 * @param {'string'} createElement 
 * @returns 
 */
function _ce( el ) {
    return document.createElement( el );
};