'use strict'
require( 'dotenv' ).config();
const bcrypt = require( 'bcrypt' );
const express = require( 'express' );
const router = express.Router();
const { creationMail } = require( './mailer' );
const { DB_userQuery, DB_createUser, DB_getAllImages, DB_updateUserMail, DB_updateScore, DB_updatePassword, DB_updateImageName, DB_resetPasswordRequest, DB_resetPasswordSet, DB_removeImage, DB_rotateImage, DB_getUserImages } = require( './database.js' );
const { CLDNRY_uploadFile, CLDNRY_deleteImage } = require( './cloudinary.js' );
const fileUpload = require( 'express-fileupload' );
const passport = require( 'passport' );
const LocalStrategy = require( 'passport-local' ).Strategy;
const session = require( 'express-session' );
const { route } = require('express/lib/application');

// Session Store
const MongoDBStore = require( 'connect-mongodb-session' )( session );
const store = new MongoDBStore(
    {
        uri: `mongodb+srv://admin:${process.env.DB_PASSWORD}@cluster0.os60q.mongodb.net/photografy`,
        collection: 'sessions'
    }
);

store.on( 'error', ( err ) => {
    console.log( 'MongoDBStore error', err );
});

router.use( session({ 
    secret: process.env.SESSION_SECRET,
    store: store,
    cookie: {
        maxAge: 1000*60*60*24 // 24h
    },
    resave: true,
    saveUninitialized: true
}));

router.use( fileUpload(
    {
        createParentPath: true, 
        safeFileNames: true,
        preserveExtension: true
    }
));

router.use( express.urlencoded( {extended:false} ) );
/////////////////////////////////////////////////////////////////////
// Passport \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////

passport.serializeUser( (user, done) => {
    user = {
        id: user._id.toString(),
        username: user.username,
        email: user.email
    }
    done( null, user );
});

passport.deserializeUser( (user, done) => {
    done( null, user );
});

passport.use( new LocalStrategy( { passReqToCallback: true }, async (req, username, password, done) => {
    console.log( 'passport local strategy' );
    console.log( 'new login request ......................................................' );
    let userdata = await DB_userQuery( username );
    if ( userdata === false ) {
        console.log( 'no user ............' );
        done( null, false );
    };
    if ( userdata ) {
        console.log( 'user found. checking password ..........................................' );
        let result = await bcrypt.compare( password, userdata.password );
        console.log( result );
        if ( result ) {
            console.log( 'password correct logging in.........' )
            return done( null, userdata );
        } else {
            console.log( 'password incorrect ..........................................' )
            return done( null, result );
        };
    }; // if userdata true
})); // LocalStategy 

const verifyUser = (req, res, next) => {
    if ( req.user ) {
        next();
    } else {
        console.log( 'user not verified' );
        res.redirect( '/index.html' );
    };
};

const unverifyUser = (req, res, next) => {
    if ( !req.user ) {
        next();
    } else {
        res.redirect( '/member.html' );
    }
};


router.use( passport.initialize() );
router.use( passport.session() );    

/////////////////////////////////////////////////////////////////////
// ROUTES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Index \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
// router.use( '/', unverifyUser );
router.get( '/', (req, res) => {
    res.redirect( '/index.html' );
});

router.get( '/locked', ( req, res ) => {
    console.log( 'locked' );
    res.send( '/locked.html' );
});


/////////////////////////////////////////////////////////////////////
// Login \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.post( '/login', passport.authenticate( 'local' ), ( req, res ) => {
    let response = {
        target: '/member.html',
        user: req.session.passport.user.username
    };
    res.send( response );
}); // login


/////////////////////////////////////////////////////////////////////
// Register \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.post( '/register', async ( req, res ) => { 
    await DB_createUser( req.body ) // database.js
        .then( (data) => {
            console.log( 'creation', data );
            if ( data === 'true' ) {
                creationMail( req.body.newEmail, req.body.newUsername );
                res.status( 201 );
                res.end( data );
            };
            if ( data === 'false' ) {
                res.status( 200 );
                res.end( data );
            };
        })
        .catch( err => {
            console.error( err );
            res.end( err );
        });
    // createNewUser();
}); // register

/////////////////////////////////////////////////////////////////////
// Member \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.use( ['/member.html', '/member'], verifyUser );
router.get( '/member', (req, res) => {
    res.redirect( '/member.html' );
}); // member

/////////////////////////////////////////////////////////////////////
// PW Reset \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.get( '/pwreset', async (req, res) => {
    console.log( 'xhr /pwreset' );
    await DB_resetPasswordRequest( req.query.reset );
    res.status(200);
    res.send( '/pwreset' );
}); // pwreset

router.get( '/resetPassword/:token/:email', async (req, res) => {
    res.redirect( `/reset.html?token=${req.params.token}?email=${req.params.email}` );
});

router.post( '/setNewPassword', async (req, res) => {
    await DB_resetPasswordSet( req.body.email, req.body.newPassword );
    res.redirect( '/index.html' );
});

/////////////////////////////////////////////////////////////////////
// Load Content \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.get( '/member/getUser', async (req, res) => {
    let userQuery = await DB_userQuery( req.query.username );
    if ( userQuery == false ) {
        console.log( 'login incorrect' );
        res.end( 'login incorrect' );
    } else {
        delete userQuery.password;
        res.set('Content-Type', 'text/plain');
        res.end( JSON.stringify( userQuery ) );
    }
}); // getUserImages

router.get( '/member/getAllImages', async (req, res) => {
    let allImages = await DB_getAllImages();
    res.set('Content-Type', 'text/plain');
    res.end( allImages );
});

router.get( '/member/showUserImages', async (req, res) => {
    let userImages = await DB_getUserImages( req.query.username );
    res.set('Content-Type', 'text/plain');
    res.end( JSON.stringify( userImages.photos.reverse() ) );
});

/////////////////////////////////////////////////////////////////////
// Update Email \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.put( '/member/updateUser', async (req, res) => {
    let result = await DB_updateUserMail( req.user.username, req.query.updateEmail );
    res.end( result.toString() );
});

/////////////////////////////////////////////////////////////////////
// Update Score \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.put( '/member/updateScore', async (req, res) => {
    await DB_updateScore( req.query.vote, req.user.username );
    res.end( 'DB updated' );
});

/////////////////////////////////////////////////////////////////////
// Update Password \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.post( '/member/changePassword', async (req, res) => {
    let passwordChangeResult = await DB_updatePassword( req.body.username, req.body ) + '';
    console.log( 'router passwordChangeResult', passwordChangeResult );
    res.end( passwordChangeResult );
});

/////////////////////////////////////////////////////////////////////
// Update Name \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.put( '/member/changeImage', async (req, res) => {
    await DB_updateImageName( req.user.username, req.query.imageId*1, req.query.newImageName );
    res.end();
});

/////////////////////////////////////////////////////////////////////
// Logout \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.get( '/logout', (req, res) => {
    req.logout();
    res.redirect( '/index.html' );
});

/////////////////////////////////////////////////////////////////////
// Delete Image \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////

router.delete( '/member/deleteImage', async (req, res) => {
    await CLDNRY_deleteImage( req.query.publicId );
    await DB_removeImage( req.query.imageId, req.user.username );
    res.end();
});

/////////////////////////////////////////////////////////////////////
// Rotate Image \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.put( '/member/rotateImage', async ( req, res ) => {
    await DB_rotateImage( req.user.username, req.query.oldUrl, req.query.newUrl );
    res.end();
});

/////////////////////////////////////////////////////////////////////
// Cloudinary \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////////////
router.post( '/member/fileupload', (req, res) => {
    console.log( '/fileupload' );
    try {
        if ( !req.files ) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            let file = req.files.image;
            let imageName = req.body.imageName;
            let username = req.body.username;
            let uploadPath = __dirname + '/tmp/' + imageName;
            let image = { imageName, uploadPath, username };
            file.mv( uploadPath, async (err) => {
                if (!err) {
                    let imageObj = await CLDNRY_uploadFile( image );
                    if ( imageObj ) {
                        res.end( JSON.stringify( imageObj ) );
                    };
                } else {
                    console.error( 'file.mv error', err );
                }
            });
        };
    } catch ( err ) {
        res.status( 500 ).send( err );
    };
});

module.exports = router;