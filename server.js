'use strict'
const routes = require( './routes' );

// NPM
const express = require( 'express' );
const favicon = require( 'express-favicon' );
const cors = require( 'cors' );
require( 'dotenv' ).config();
const app = express();
const PORT = process.env.PORT || 9027;

// Middleware
app.use( routes );
app.use( cors() );
app.use( express.static( 'www' ) ); 
app.use( favicon(__dirname + '/www/assets/favicon.png') );


// Basic Server Startup
const server = app.listen( PORT, ()=>{
    console.log( '........................................................................' );
    console.log( 'Server up on Port ' + PORT + ' .................................................' ); 
    console.log( '........................................................................' );

});